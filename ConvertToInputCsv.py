import json
import csv
from progress import progress

with open('scraped_articles.json') as data_file:
    articles = json.load(data_file)

csvfile = "inputValues.csv"

print("Creating ", csvfile)
total = len(articles['articles'])
progress(0, total, status="Start")

with open(csvfile, "w") as output:
    writer = csv.writer(output, lineterminator='\n')
    count = 0

    for article in articles['articles']:
        if count == 0:
            header = article.keys()
            writer.writerow(header)
            count += 1
        writer.writerow(article.values())
        count += 1
        progress(count, total, status="Doing")

progress(total, total, status="Done.")
