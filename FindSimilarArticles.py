import spacy
import json

# Load English tokenizer, tagger, parser, NER and word vectors
nlp = spacy.load('en_core_web_sm')

with open('scraped_articles.json') as data_file:
    articles = json.load(data_file)

docs = []

for article in articles['articles']:
    print("Processing article", article["id"])
    # doc = nlp(article['text'])
    # doc = nlp(article['title'])
    doc = nlp(article['summary'])
    # doc = nlp(''.join(article['keywords']))
    for entity in doc.ents:
        if entity.label_ == "PERSON":
            print(entity.text, entity.label_)
    id = article["id"]
    result= {
        "id": article["id"],
        "doc": doc
    }
    docs.append(result)

scores = []

print("Comparing articles")

for processedDoc in docs:
    docScores = []
    for comparingDoc in docs:
        if processedDoc["id"] != comparingDoc["id"]:
            similarity = processedDoc["doc"].similarity(comparingDoc["doc"])
            if similarity > 0.9:
                score = {
                    "id": comparingDoc["id"],
                    "similarity": round(similarity,2)
                    }
                docScores.append(score)

    scores.append({
        "id": processedDoc["id"],
        "similarities": docScores
    })

try:
    with open('similarities.json', 'w') as outfile:
        json.dump(scores, outfile)
except Exception as e: print(e)

print("Done!")
#
# # Process whole documents
# text = (u"When Sebastian Thrun started working on self-driving cars at "
#         u"Google in 2007, few people outside of the company took him "
#         u"seriously. “I can tell you very senior CEOs of major American "
#         u"car companies would shake my hand and turn away because I wasn’t "
#         u"worth talking to,” said Thrun, now the co-founder and CEO of "
#         u"online higher education startup Udacity, in an interview with "
#         u"Recode earlier this week.")
# doc = nlp(text)
#
# # Find named entities, phrases and concepts
# for entity in doc.ents:
#     print(entity.text, entity.label_)
#
# # Determine semantic similarities
# doc1 = nlp(u"my fries were super gross")
# doc2 = nlp(u"such disgusting fries")
# similarity = doc1.similarity(doc2)
# print(doc1.text, doc2.text, similarity)
