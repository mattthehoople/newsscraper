import React, { Component } from 'react';
import { render } from "react-dom";
import logo from './logo.svg';
import './App.css';

import articles from './scraped_articles.json';

import ReactTable from "react-table";
import "react-table/react-table.css";

class App extends Component {
    constructor() {
        super();
        this.state = {
            data: articles.articles
        };
    }

    render() {
        const { data } = this.state;
        return (
            <div className="App">
              <div>
                      <ReactTable
                        data={data}
                        columns={[
                              {
                                Header: "ID",
                                accessor: "id"
                              },
                              {
                                Header: "Source",
                                accessor: "source"
                              },
                              {
                                Header: "Title",
                                accessor: "title"
                              },
                              {
                                Header: "Thumbnail",
                                accessor: "top_image",
                                Cell: props => <img height='40px' src={props.value} />
                              },
                        ]}
                        defaultPageSize={10}
                        className="-striped -highlight"
                      />
                      <br />
                    </div>
            </div>
        );
    }
}

export default App;
