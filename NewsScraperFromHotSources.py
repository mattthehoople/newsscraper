import feedparser as fp
import json
import newspaper
from newspaper import Article
from time import mktime
from datetime import datetime

# We're going to stick with a hard coded list of news ources for now

# hotData = {}
#
#
# hot = newspaper.hot()
# popular = newspaper.popular_urls()
# hotData['terms'] = hot
# hotData['sources'] = popular
#
# try:
#     with open('hot.json', 'w') as outfile:
#         json.dump(hotData, outfile)
# except Exception as e: print(e)


# Set the limit for number of articles to download
LIMIT = 4

data = {}
data['articles'] = []

# Loads the JSON files with news sites
with open('hot.json') as data_file:
    companies = json.load(data_file)

count = 1

articleId = 1

# Iterate through each news company
for source in companies['sources']:

    # if articleId > 20:
    #     break
    print("Building site for ", source)
    paper = newspaper.build(source, memoize_articles=False)

    noneTypeCount = 0
    for content in paper.articles:
        if count > LIMIT:
            break
        try:
            content.download()
            content.parse()
            content.nlp()
        except Exception as e:
            print(e)
            print("continuing...")
            continue
        # Again, for consistency, if there is no found publish date the article will be skipped.
        # After 10 downloaded articles from the same newspaper without publish date, the company will be skipped.
        if content.publish_date is None:
            print(count, " Article has date of type None...")
            noneTypeCount = noneTypeCount + 1
            if noneTypeCount > 10:
                print("Too many noneType dates, aborting...")
                noneTypeCount = 0
                break
            count = count + 1
            continue
        article = {}
        article['id'] = articleId
        article['title'] = content.title
        article['summary'] = content.summary
        article['keywords'] = content.keywords
        article['text'] = content.text
        article['link'] = content.url
        article['top_image'] = content.top_image
        article['published'] = content.publish_date.isoformat()
        article['source'] = source
        data['articles'].append(article)
        print(count, "articles downloaded from", source, " using newspaper, url: ", content.url)
        count = count + 1
        articleId = articleId + 1
        noneTypeCount = 0

    count = 1

# Finally it saves the articles as a JSON-file.
try:
    with open('scraped_articles.json', 'w') as outfile:
        json.dump(data, outfile)
except Exception as e: print(e)
